{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# GPyTorch Regression Tutorial (GPU)\n",
    "\n",
    "(This notebook is the same as the [simple GP regression tutorial](../01_Exact_GPs/Simple_GP_Regression.ipynb) notebook, but does all computations on a GPU for acceleration.\n",
    "Check out the [multi-GPU tutorial](./Simple_MultiGPU_GP_Regression.ipynb) if you have large datasets that needs multiple GPUs!) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In this notebook, we demonstrate many of the design features of GPyTorch using the simplest example, training an RBF kernel Gaussian process on a simple function. We'll be modeling the function\n",
    "\n",
    "\\begin{align}\n",
    "  y &= \\sin(2\\pi x) + \\epsilon \\\\ \n",
    "  \\epsilon &\\sim \\mathcal{N}(0, 0.2) \n",
    "\\end{align}\n",
    "\n",
    "with 11 training examples, and testing on 51 test examples."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import torch\n",
    "import gpytorch\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "%load_ext autoreload\n",
    "%autoreload 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set up training data\n",
    "\n",
    "In the next cell, we set up the training data for this example. We'll be using 11 regularly spaced points on [0,1] which we evaluate the function on and add Gaussian noise to get the training labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Training data is 11 points in [0,1] inclusive regularly spaced\n",
    "train_x = torch.linspace(0, 1, 100)\n",
    "# True function is sin(2*pi*x) with Gaussian noise\n",
    "train_y = torch.sin(train_x * (2 * math.pi)) + torch.randn(train_x.size()) * 0.2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Setting up the model\n",
    "\n",
    "See [the simple GP regression tutorial](../01_Exact_GPs/Simple_GP_Regression.ipynb) for a detailed explanation for all the terms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class ExactGPModel(gpytorch.models.ExactGP):\n",
    "    def __init__(self, train_x, train_y, likelihood):\n",
    "        super(ExactGPModel, self).__init__(train_x, train_y, likelihood)\n",
    "        self.mean_module = gpytorch.means.ConstantMean()\n",
    "        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())\n",
    "    \n",
    "    def forward(self, x):\n",
    "        mean_x = self.mean_module(x)\n",
    "        covar_x = self.covar_module(x)\n",
    "        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)\n",
    "\n",
    "# initialize likelihood and model\n",
    "likelihood = gpytorch.likelihoods.GaussianLikelihood()\n",
    "model = ExactGPModel(train_x, train_y, likelihood)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the GPU\n",
    "\n",
    "To do computations on the GPU, we need to put our data and model onto the GPU. (This requires PyTorch with CUDA)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "train_x = train_x.cuda()\n",
    "train_y = train_y.cuda()\n",
    "model = model.cuda()\n",
    "likelihood = likelihood.cuda()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That's it! All the training code is the same as in [the simple GP regression tutorial](../01_Exact_GPs/Simple_GP_Regression.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Training the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Iter 1/50 - Loss: 0.930   lengthscale: 0.693   noise: 0.693\n",
      "Iter 2/50 - Loss: 0.899   lengthscale: 0.644   noise: 0.644\n",
      "Iter 3/50 - Loss: 0.865   lengthscale: 0.598   noise: 0.598\n",
      "Iter 4/50 - Loss: 0.828   lengthscale: 0.555   noise: 0.554\n",
      "Iter 5/50 - Loss: 0.786   lengthscale: 0.514   noise: 0.513\n",
      "Iter 6/50 - Loss: 0.739   lengthscale: 0.475   noise: 0.474\n",
      "Iter 7/50 - Loss: 0.689   lengthscale: 0.439   noise: 0.437\n",
      "Iter 8/50 - Loss: 0.638   lengthscale: 0.405   noise: 0.402\n",
      "Iter 9/50 - Loss: 0.590   lengthscale: 0.372   noise: 0.369\n",
      "Iter 10/50 - Loss: 0.545   lengthscale: 0.342   noise: 0.339\n",
      "Iter 11/50 - Loss: 0.505   lengthscale: 0.315   noise: 0.310\n",
      "Iter 12/50 - Loss: 0.468   lengthscale: 0.292   noise: 0.284\n",
      "Iter 13/50 - Loss: 0.432   lengthscale: 0.273   noise: 0.259\n",
      "Iter 14/50 - Loss: 0.397   lengthscale: 0.257   noise: 0.237\n",
      "Iter 15/50 - Loss: 0.363   lengthscale: 0.244   noise: 0.216\n",
      "Iter 16/50 - Loss: 0.329   lengthscale: 0.233   noise: 0.196\n",
      "Iter 17/50 - Loss: 0.296   lengthscale: 0.225   noise: 0.179\n",
      "Iter 18/50 - Loss: 0.263   lengthscale: 0.219   noise: 0.163\n",
      "Iter 19/50 - Loss: 0.230   lengthscale: 0.215   noise: 0.148\n",
      "Iter 20/50 - Loss: 0.198   lengthscale: 0.213   noise: 0.134\n",
      "Iter 21/50 - Loss: 0.166   lengthscale: 0.213   noise: 0.122\n",
      "Iter 22/50 - Loss: 0.136   lengthscale: 0.214   noise: 0.111\n",
      "Iter 23/50 - Loss: 0.106   lengthscale: 0.216   noise: 0.101\n",
      "Iter 24/50 - Loss: 0.078   lengthscale: 0.219   noise: 0.092\n",
      "Iter 25/50 - Loss: 0.052   lengthscale: 0.224   noise: 0.083\n",
      "Iter 26/50 - Loss: 0.028   lengthscale: 0.229   noise: 0.076\n",
      "Iter 27/50 - Loss: 0.006   lengthscale: 0.235   noise: 0.069\n",
      "Iter 28/50 - Loss: -0.013   lengthscale: 0.242   noise: 0.064\n",
      "Iter 29/50 - Loss: -0.029   lengthscale: 0.249   noise: 0.058\n",
      "Iter 30/50 - Loss: -0.042   lengthscale: 0.256   noise: 0.053\n",
      "Iter 31/50 - Loss: -0.052   lengthscale: 0.263   noise: 0.049\n",
      "Iter 32/50 - Loss: -0.058   lengthscale: 0.269   noise: 0.046\n",
      "Iter 33/50 - Loss: -0.062   lengthscale: 0.274   noise: 0.042\n",
      "Iter 34/50 - Loss: -0.063   lengthscale: 0.278   noise: 0.040\n",
      "Iter 35/50 - Loss: -0.062   lengthscale: 0.280   noise: 0.037\n",
      "Iter 36/50 - Loss: -0.060   lengthscale: 0.281   noise: 0.035\n",
      "Iter 37/50 - Loss: -0.057   lengthscale: 0.280   noise: 0.033\n",
      "Iter 38/50 - Loss: -0.054   lengthscale: 0.278   noise: 0.032\n",
      "Iter 39/50 - Loss: -0.051   lengthscale: 0.275   noise: 0.031\n",
      "Iter 40/50 - Loss: -0.048   lengthscale: 0.270   noise: 0.030\n",
      "Iter 41/50 - Loss: -0.046   lengthscale: 0.265   noise: 0.030\n",
      "Iter 42/50 - Loss: -0.045   lengthscale: 0.259   noise: 0.029\n",
      "Iter 43/50 - Loss: -0.045   lengthscale: 0.252   noise: 0.029\n",
      "Iter 44/50 - Loss: -0.045   lengthscale: 0.246   noise: 0.029\n",
      "Iter 45/50 - Loss: -0.046   lengthscale: 0.241   noise: 0.029\n",
      "Iter 46/50 - Loss: -0.047   lengthscale: 0.236   noise: 0.030\n",
      "Iter 47/50 - Loss: -0.049   lengthscale: 0.233   noise: 0.030\n",
      "Iter 48/50 - Loss: -0.052   lengthscale: 0.230   noise: 0.031\n",
      "Iter 49/50 - Loss: -0.054   lengthscale: 0.228   noise: 0.031\n",
      "Iter 50/50 - Loss: -0.057   lengthscale: 0.228   noise: 0.032\n"
     ]
    }
   ],
   "source": [
    "# Find optimal model hyperparameters\n",
    "model.train()\n",
    "likelihood.train()\n",
    "\n",
    "# Use the adam optimizer\n",
    "optimizer = torch.optim.Adam(model.parameters(), lr=0.1)  # Includes GaussianLikelihood parameters\n",
    "\n",
    "# \"Loss\" for GPs - the marginal log likelihood\n",
    "mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)\n",
    "\n",
    "training_iter = 50\n",
    "for i in range(training_iter):\n",
    "    # Zero gradients from previous iteration\n",
    "    optimizer.zero_grad()\n",
    "    # Output from model\n",
    "    output = model(train_x)\n",
    "    # Calc loss and backprop gradients\n",
    "    loss = -mll(output, train_y)\n",
    "    loss.backward()\n",
    "    print('Iter %d/%d - Loss: %.3f   lengthscale: %.3f   noise: %.3f' % (\n",
    "        i + 1, training_iter, loss.item(),\n",
    "        model.covar_module.base_kernel.lengthscale.item(),\n",
    "        model.likelihood.noise.item()\n",
    "    ))\n",
    "    optimizer.step()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Make predictions with the model\n",
    "\n",
    "First, we need to make some test data, and then throw it onto the GPU"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "test_x = torch.linspace(0, 1, 51).cuda()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the rest of the code follows [the simple GP regression tutorial](../01_Exact_GPs/Simple_GP_Regression.ipynb)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get into evaluation (predictive posterior) mode\n",
    "model.eval()\n",
    "likelihood.eval()\n",
    "\n",
    "# Test points are regularly spaced along [0,1]\n",
    "# Make predictions by feeding model through likelihood\n",
    "with torch.no_grad(), gpytorch.settings.fast_pred_var():\n",
    "    observed_pred = likelihood(model(test_x))\n",
    "    mean = observed_pred.mean\n",
    "    lower, upper = observed_pred.confidence_region()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For plotting, we're going to grab the data from the GPU and put it back on the CPU.\n",
    "We can accomplish this with the `.cpu()` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean = mean.cpu()\n",
    "lower = lower.cpu()\n",
    "upper = upper.cpu()\n",
    "\n",
    "train_x = train_x.cpu()\n",
    "train_y = train_y.cpu()\n",
    "test_x = test_x.cpu()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAQMAAADGCAYAAADWg+V4AAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuNCwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8QVMy6AAAACXBIWXMAAAsTAAALEwEAmpwYAAA1yUlEQVR4nO2dd3hUVdrAf2cmk0x6I40EkE4IJHRBkKaIi4gLglRdu+CiuCi6FgREd91VsKIr66qoNEVEF9EPWEE60jsECC2kF9LLZOZ8f0wxZZJMyCRM8PyeZ57M3Hvuue+9mXnvOe95i5BSolAoFJprLYBCoXANlDJQKBSAUgYKhcKCUgYKhQJQykChUFhQykChUABOUAZCCL0Q4lchxCEhxDEhxDxnCKZQKBoXUV8/AyGEALyllPlCCB2wDZghpdzlDAEVCkXj4FbfDqRZm+RbPuosL+XJpFA0MZxiMxBCaIUQB4E0YIOUcrcz+lUoFI1HvUcGAFJKI9BNCBEAfCuE6CKlPFq+jRDiUeBRAG9v756dOnVyxqkVCkUd2LdvX4aUMsTevnrbDKp0KMTLQKGU8s3q2vTq1Uvu3bvXqedVKBS1I4TYJ6XsZW+fM1YTQiwjAoQQnsAw4GR9+1UoFI2LM6YJEcASIYQWs3L5Skq51gn9KhSKRsQZqwmHge5OkEWhUFxDnGJAVFy/GAwGEhMTKS4uvtaiKOqAXq8nKioKnU7n8DFKGShqJDExEV9fX2644QbM/mUKV0dKSWZmJomJibRu3drh41RsgqJGiouLCQ4OVoqgCSGEIDg4uM6jOaUMFLWiFEHT42r+Z0oZKFyexMRE7rrrLtq3b0/btm2ZMWMGpaWlAHz22WdMnz79GktYFR8fH7vbtVot3bp1IyYmhri4OBYsWIDJZKqxr/Pnz7Ns2bKGELMCShkonE5ycjKDBg0iJSWl3n1JKRkzZgx//OMfOX36NPHx8eTn5/Piiy86QVL7lJWVNVjfnp6eHDx4kGPHjrFhwwZ+/PFH5s2rOdC3sZQBUspGf/Xs2VMqmgbHjx+v8zHTpk2TGo1GTps2rd7n37hxo7z55psrbMvJyZFBQUGyoKBAfvrpp3LUqFFy0KBBsl27dnLu3LlSSinz8/PliBEjZGxsrIyJiZErVqyQUkq5d+9eOXDgQNmjRw952223yaSkJCmllIMGDZIzZsyQPXv2lHPnzpUtW7aURqPR1ldUVJQsLS2VZ86ckcOHD5c9evSQAwYMkCdOnJBSSpmQkCD79u0ru3TpIl988UXp7e1t93oqbz979qwMCgqSJpNJnjt3Tg4YMEB2795ddu/eXW7fvl1KKeWNN94o/fz8ZFxcnFy4cGG17Spj738H7JXV/C6VMlDUSF2UgV6vl5gjViu89Hr9VZ//nXfekU899VSV7d26dZOHDh2Sn376qQwPD5cZGRmysLBQxsTEyD179shVq1bJhx9+2Nb+ypUrsrS0VPbr10+mpaVJKaVcsWKFfOCBB6SUZmVQXnmNGjVK/vzzz7Z2Dz30kJRSyqFDh8r4+HgppZS7du2SQ4YMkVJKeeedd8olS5ZIKaV8//33HVYGUkrp7+8vU1JSZEFBgSwqKpJSShkfHy+tv5NNmzbJO+64w9a+unaVqasyUNMEhdNISEhg0qRJeHl5AeDl5cXkyZM5d+5cg5532LBhBAcH4+npyZgxY9i2bRtdu3Zlw4YNPPfcc2zduhV/f39OnTrF0aNHGTZsGN26dePVV18lMTHR1s/48eMrvF+5ciUAK1asYPz48eTn57Njxw7GjRtHt27deOyxx0hOTgZg+/btTJw4EYB77733qq7DYDDwyCOP0LVrV8aNG8fx48fr1a6uKD8DhdOIiIjAz8+P4uJi9Ho9xcXF+Pn5ER4eftV9du7cmVWrVlXYlpuby8WLF2nXrh379++vYjkXQtChQwf279/PunXreOmll7jlllsYPXo0MTEx7Ny50+65vL29be9HjRrFCy+8QFZWFvv27WPo0KEUFBQQEBDAwYMH7R5/NRb8hIQEtFotoaGhzJs3j7CwMA4dOoTJZEKv19s95q233nKoXV1RIwOFU0lNTWXq1Kns2rWLqVOn1tuIeMstt1BYWMjnn38OgNFo5Omnn+b++++3jUA2bNhAVlYWRUVFrFmzhv79+5OUlISXlxdTpkxh1qxZ7N+/n44dO5Kenm5TBgaDgWPHjtk9r4+PD71792bGjBmMHDkSrVaLn58frVu35uuvvwbMU+xDhw4B0L9/f1asWAHA0qVLHbq29PR0pk6dyvTp0xFCkJOTQ0REBBqNhi+++AKj0QiAr68veXl5tuOqa1dvqps/NORL2QyaDldjQHQ2Fy9elCNHjpTt2rWTbdq0kdOnT5fFxcVSSik//fRTedddd8nBgwdXMCD+9NNPsmvXrjIuLk726tVL7tmzR0op5YEDB+TNN98sY2NjZefOneXixYullGabgbWNla+//loCcvPmzbZtCQkJcvjw4TI2NlZGR0fLefPm2bY7YkDUaDQyLi5Odu7cWcbGxso33njDZqiMj4+XXbt2lbGxsfLZZ5+19VFaWiqHDBkiY2Nj5cKFC6ttV5m62gycns/AEVQ+g6bDiRMniI6OvtZiKK4Ce/+7Bs1noFAorg+UMlAoFIBSBgqFwoJSBgqFAlDKQKFQWHBGQtQWQohNQojjlvJqM5whmEKhaFycMTIoA56WUnYG+gJ/FkJ0dkK/CgVg9uybMmWK7XNZWRkhISGMHDnyGkp1/VFvZSClTJZS7re8zwNOAJH17VehsOLt7c3Ro0cpKioCzB6HkZHqK+ZsnGozEELcgDlTsiqvpnAqI0aM4IcffgBg+fLltqAggIKCAh588EH69OlD9+7d+e677wBzHoCbb76ZHj160KNHD3bs2AHA5s2bGTx4MGPHjqVTp05MnjyZa+F852o4LVBJCOEDfAM8JaXMtbPfVl6tZcuWzjqtohF56imoJkbnqunWDd5+u/Z2EyZM4JVXXmHkyJEcPnyYBx98kK1btwLw2muvMXToUD755BOuXLlCnz59uPXWWwkNDWXDhg3o9XpOnz7NxIkTsXq+HjhwgGPHjtG8eXP69+/P9u3bGTBggHMvronhFGVgKcX+DbBUSrnaXhsp5WJgMZjdkZ1xXsXvh9jYWM6fP8/y5csZMWJEhX3r16/n+++/5803zRX9iouLuXjxIs2bN2f69OkcPHgQrVZLfHy87Zg+ffoQFRUFQLdu3Th//rxSBvXtQJjjNv8DnJBSLqy/SApXxZEneEMyatQonnnmGTZv3kxmZqZtu5SSb775ho4dO1ZoP3fu3GpDfT08PGzvtVptg6Y6ayo4w2bQH7gXGCqEOGh5jajtIIWirjz44IPMmTOHrl27Vtg+fPhw3nvvPdu8/8CBA0ADhvpepzhjNWGblFJIKWOllN0sr3XOEE6hKE9UVBRPPvlkle2zZ8/GYDAQGxtLTEwMs2fPBuDxxx9nyZIlxMXFcfLkyQrJSxRVUSHMihpRIcxNFxXCrFAorgqlDBQKBaCUgUKhsKCUgUKhAJQyUCgUFpQyUCgUgFIGiiZCSkoKEyZMoG3btvTs2ZMRI0ZUcC92lK1btxITE0O3bt24fPkyY8eOtdtu8ODB/N6Wv1VFJUWdeGtD3X+ANfGXYR1qbSOlZPTo0fzpT3+yFSo5dOgQqampdOhQ+/HlWbp0Kc8//7wtP0Llak2/Z9TIQOHybNq0CZ1Ox9SpU23b4uLiGDBgALNmzaJLly507drVVhuxuhDljz/+mK+++orZs2czefJkzp8/T5cuXQAoKipiwoQJREdHM3r0aFvuBDAHQvXr148ePXowbtw48vPzAbjhhhuYM2cOPXr0oGvXrpw8eRKA/Px8HnjgAbp27UpsbCzffPNNjf24CkoZKFyeo0eP0rNnzyrbV69ezcGDBzl06BAbN25k1qxZtkKoBw4c4O233+b48eMkJCSwfft2Hn74YUaNGsUbb7xRpQTahx9+iJeXFydOnGDevHns27cPgIyMDF599VU2btzI/v376dWrFwsX/haP16xZM/bv38+0adNsUZPz58/H39+fI0eOcPjwYYYOHVprP66AmiYomizbtm1j4sSJaLVawsLCGDRoEHv27MHPz6/OIcpbtmyxxT3ExsYSGxsLwK5duzh+/Dj9+/cHoLS0lH79+tmOGzNmDAA9e/Zk9Wpz9P7GjRtt0xmAwMBA1q5dW2M/roBSBvUgOTmZCRMmsHLlSvyDQkjKKeJKoYH8kjLyi8soKCnDJCUajcBNI9BqBD4ebgR6uxPs7U6gtzt+el2VvupTtfh6JCYmps5ze2eFKEspGTZsGMuXL6/xPLWdo7Z+XAE1TbhKyowmnnlhNlu3bmXy47P4aMtZvj+YxJb4dPZfyCY+NY/LV4pIzinmxJkLzLp/DAdPnedwYg6/nEpn9f7L/GfrOT7emsDG46k888Jstm3bxiuvvHKtL83lGDp0KCUlJSxevNi27fDhwwQEBLBy5UqMRiPp6els2bKFPn36XNU5Bg4cyLJlywDztOTw4cMA9O3bl+3bt3PmzBnAnGKttlWMYcOGsWjRItvn7Ozsq+qnsVHKoI5cvlKEh4cenZuWZZ/9ByklP3/7JX8Z1pFnR8baPWb90g84d3Qv679cVGXftFs7MywmnGWf/QeTycSHH36IEAJPT8+GvpQmgxCCb7/9lo0bN9K2bVtiYmJ4/vnnmTRpErGxscTFxTF06FD++c9/XvWoatq0aeTn5xMdHc3LL79ss1GEhITw2WefMXHiRGJjY+nXr5/NUFgdL730EtnZ2XTp0oW4uDg2bdp0Vf00NiqE2QFMJsnJlDz2XsgiM7+U3Mw0vl/8D47s2IihpBidh56u/Ycx6tHn8AsKITczjc//NpOLJw9RZiit0p+buwf/XGt+8lTuy83dA72nNzPf+oLbB95IbJQ/3h7XbjanQpibLnUNYVY2gxowmiTHk3LZcz6LnCKDbbtfcCh6Lx/KSktwc/egrLQEvZcPfkEhwG8jgZ633IXJWGZXadTUV35pCRtXf0lAVDv2Xciia1QAvVoFXlOloLj+Ud+uajiTlscv8RnkllMC5cm7kslNIyfSd8R4dq1bSW5WOs+OjKWstMTWZu/GNbb39pSGdQThrvcEISocu2PtcnasXW4bRRxJvEKXSH/6tA7Cy1392xTOR32rKpGZX8LmU+lczCqssd0Dc963vb/7iTlA1SG/zkOPl28A7bv3Y9CY+21Kw4p1BNHvjgnMWfpLtVMPAINRcuDiFY4n59K3TTDdogLQaEQD3AHF7xVnpUr/BBgJpEkpuzijz8bGYDSxdudxZj3+IPe++Jbt6V0X7A35Y/oOYeyTc4HflEblEYR1FCA0GpDS7ijCSnpKCuOenMSMvy3ij/1iaBnsdfUX7SBSSsxJsBVNhauxBTprNeEz4HYn9dXoJOcUsWz3Rd78x99IqMbqXxO5mWm8//QUcrPSbdOHGe98xU0jJ5KXnVGl3Yx3VtJjyEh0HubU3ToPPT2G3knHngOqPdaKdTSx8qO3+GZ/IuuOJFNY2nBpvvV6PZmZmariUBNCSklmZmaF1PCO4LTVBEtptbWOjAxcZTWhzGhiV0IWQ7pEVXhSWylv9S+Pda5/n2UEserduez8YQX97pjAyIfnkZOhIyfDjZTzhWz770pu/uN4fAN9+PWnZZzcu5Ibb++KVlvEznUr0ercMRpK6XfHBNsIwh6VRxPlZXxv/TEGdwyhU7hffW6HXQwGA4mJiRQXFzu9b0XDodfriYqKQqfTVdhe02pCoymDSuXVel64cMEp571asgpK+eFIMhl5JbUuFVbG+uOXuIGMBQZaXv0BR6cXl4BT3Hh7SwpzlyPZyYNz3622tSMytgnx5pboMHzUqoOiGlxiadGVyqsdS8ph86l0SstMQO1LhVaevaOrxW/gJuBD4B4g0LL3DELzIxGtTaSc/xmT8TyQBBgAd8AD0AMt0Wi7EBAyGA+vvuxZ743JNAe9t5El8wvp0j+f2AH56Nyr3qLTh3bXKGNCegHJORe4NTqMdqE+zr1piuue39UjpLTMxM8n0ziRXKUurN2lwvLk52gIbfEpSQk3Am2BAoTmO3z8d5CX/S1u7pkYDaXcED2BR1993PIU32N7ivsHh5GZfByt7ihGwxo69TrD2CejKMrXEL/fi5N7vTmxx5tDW3351tdIr1tz6Tsih/BWZqel9Us/IC8rnbBW7Zjy1wV2ZQQoKjXy30NJdIn0Z1CHENzdlJOpwjF+NzaDrIJS1h5OIjO/qkdgTeRmaZk36Tuk6VHAC/gZ+BxYDRQQFB5FdO+B9B0xni2rP+PY7k08u3gt6794v4JNIDAs0m678k92kwnOHPRi14/+HNnug7FMIMRmpJwL/FJBLq3OnVad4mx2C3sEeun4Q9cIwvzqZkhSXL80eBEVIcRyYCfQUQiRKIR4yBn9Ooszafks//VinRRB/hUtqxeF8Op9rUH+haCwPbjpegLDEJpldOrdk17DRhPZNpq7n5hDZNtO6Dz0FOXlsP7LRVVWFaprVx6NBjr0KOS+F5N5eWkCIx9Ox9v/JmAzQvwCDLKtPHQfPKLaeAcr2YUGVu65xL4L2bZtycnJDBo0iJSUlDreRcX1znUdmyClZMfZTDbuO8nnr81k9OMv8u0Hr9X4NDUaYcd/A/jp82BKCjX0GpbLLROy+GXVC9Va/2uy9JdfjXC0Hfy2YjHy4dl89PxOSgqnA80xj0yeAo7U2kd52oR4MzwmnJkznuCjjz7iscce44MPPqi2veL65HdZXq3YYGTNwcv8ei6L9V+a1+a/fP2ZGp+mZw57svDxVnz7QSgt2hcz7Z/7yUgahodnUo3+Ay8t2WjXb+Clz/9XoX9H20E5f4KFMykp/DuhLW5jyLgjaHW9gQNoNB8CQTX2UZ6xN7bF092NDz/8UEVHKuxyXRoQM/NL+P5QEo8Oja7wJE69YI4lr+z3X1yg4bvFIez+0Z/AMAP3v5xE1/75fPPeAs4d3cvaj9+kIPcKdz8xB7+gEJsnoRVHVyMcaVd59GCVOe3SMdIuxaLVhRPe6jNSLjwCjMNQ8hIenrm1eky+tGRjhaVJLy8vRo8ebUvVpVBcNyMD61x459GzrNhziSuFBtuT2M3do0JbN3cP29P09AFP/vFIFLt/9KH/nYk89/F5vny9DU8P78iOtcuRUrJ34xoSjuxh3qSB1Z6/ppFDXdrVJvPsL1YRErWAboNeJKq9BD7k8NbnSL9c0bnEitXrESEqKKKioiK8fXxUViWFjevGZjBt2jQ++mgxN40cz91PzLVtX/XOHHauWwlCIE0mm/9/n9vvQ+f+Ftu+C0TvnUxxwd3cNPIGxj451+bgs3/TWrvnqm1+Xl+qk7myl6KUsPsnP75fHIKxTPCHP2UycHQ2Gm25vt41O0j1vOUujv+6mc43DrEFTRXlZPLj2jWE+qrVht8LjeKBWBecqQw8PT3tuspaf7CfzpuOX1AIqZcSyM/OxCcwGN+AARz/9XFKClsDbwMvAL+lxtbq3PHy9ScvKx0hNEhpdk6qzTNRqxH46t3w8XCz/NWhEVBmkhilxGSS5BWXkVVQSm6xgepuvT2Zw1q0ITcrvUK0pJWcTC2r3gnj2C4fWkUXMenZFN54LNqusVJoNCz46YTts04rGNIplJjm/jXeZ8X1wXWtDI6fvcBD055k35b1DrkS71nvxzfvheKuNxHR5lVOH5iHRqPFZDLajtVotezdsIawVu0Ia9mOw1t/QggNUPHp7O6mIcJfT2SAJ5GBnoT76XHTOjbzKjOayC40kHSliPOZBSRmF9k8Iq8GKeHAZl++eT8Uo0Fw+31nuRQ/kwObHRvddIn0Z0jHEIflVzRNXMIduSE4k5bPposGNO5etRrvSooE37wXyt6N/gixmdKSiZw+YF5rN5mMABhKitn/839tx6ReOEPqhTMIjYaZi761ef21CvYiOsKPdqE+6K7yx+Om1RDi60GIrwdxLQIwmiRJV4o4lZLHqdS8OisGIaDHkDxCIi/x4XOC7xf3JihsNrATIbLtjm7Kc/RyDml5xYzs2hx/L/v2B8X1TZN8DJhMki3x6aw9nERpmalWo1xGko53n2rJvv/5cduUTGZ/6UaPIb1sS3xCo6VT74HEDvwD/s3Cqyz9zVm2hXbRMbz59rts3/gDY3pEER3hd9WKwB5ajaBFkBe3dg7jkZvbcGt02FV5Du7+6W2KC/rSKvpLstO6ofM4TesuT5uvU2hsyhIpbWHXVtJyS1j260XOprtWpR9F49BkpgnWugIfL/mS/emSpCuOhdQe/9Wbpa+HIwTc+3wyHXsVkpuZxoI/jyE/O6OKE5HVeGfdfvOdE1nwznvERgVcEz//xOxCdiVkcamWzEv2HZrigJVAO1p0WMPYGTp+/ck8uvENbGYLu64cOi0E9GoVxE1tg1U2peuM68LpaP78+Wzdto2HZvy1RkVgXUq7kpHO+i+D+M/s5gSFGfjL+xfo2Mv8g7IG/YS2bFtlNGEdZTyz6GvunvwAgZpCet0QdM0CfqICvRjbM4p7eregZVDVrEbVJUxxc/fAx/8ST7x9hl63FnAp/m7W/vtWdv20nSPbN9iWTXesXc7M2yqmeZcS9pzP4pv9iRSUNFziFIVr4fIjg9pWCyqz6t257Fi7lqDwjWSl9KHXrbmMfTIVd710yB1YCOgU7sfN7Zu5ZDbii5mFbI5Ps8VZlF86PLV/u220Y73Om0ZOZOyTc/l1vR+r3wtF51FGROt5XDi50CGDq7eHlj90iaCFHUWkaHo06dWEPcfOMv2pmRzYWvNqwW8/9NbAd0A08DRa3Ue88YP9GgWV+wrx9WBop1CaB7i2i67JJPH08qS0pKpiqw43dw9mLjrJklcjSL2gA+ag1b2JqaykwlShchYnMCvIvm2CubF1kMqF6IIYTRKtg9O5JjtN2HQyjW1JRtw8ql8tKD9Mbt/tBWAPEInGbRQ+/l/y1Htf2fqrzh04sFko/doGM6lPS5dXBAAajeD8uXPcM2FCrW3Lxy6EtyrlqXcvEhCyCZhPyw7n6TZwCge3/GgzJNqr/iQl7DybybcHLjdovkVF3SktM/HdwctOyVHp0srgdFoeULML7/qlH5BwZC+rF2Vx+tArQApa3QBMZT+Sn5PFzrUrKvRZua+SvCwm9G5B3zZNy1gWERFBUEAAQgg0mt/+jToPPc2at0IIYVd5enhKZn/ZgrFPpnIxPoRjuxdSmNuBeZMGMvO2jjXaEi5kFrJ010UuZtZszFQ0DoWlZazal8iR+PMMHjy43mHpLj1NWLzlLAUlRrv7fpsW6ID3gMeA/wKTgbwq7SvbGKwW835tgx0eYrkaY8aMISIigrS0NFatWmVzWy6fSMXqG1HZc9F8/2KAVUAk5rDoD237a7IlCAG9bwiiXxNToNcTOYUGvj2QSHahgVXvzmXXupUOhaU3WZtBTcogNzONb97/kKM7piHlADTafxI3cAdD73mATV99XGPiUC93Lbd3CadVsLdTr+taYVUKjz76KAveXcTR0xe4d/Z7NR5jtZ8c3r6PstJ/A3fg4bmGkqIpuLmXOZSxuXmAntu7RODvqZyUGpO03GLWHLzMn4fF2DWI6/V6ioqK7BzZhG0GNZF3JYr4/W8jZU802vuQpr/i6e1FZNvoGsOEWwR5Mblvq+tGEQCsXr2aRYsWERcXx+f/Wcz2jetqTYhqtZ8YDSlodWOBFykpGoXe+zhTnl9XY+Sl1U6zZ98BevXtz5ZDpxvgqhT2OJuez9f7EikoMVbJj+Hl5cXkyZM5d+7cVfXtemtntZCbmcYHz64jK3U+kEXsza8xbNJYdq0z2oxg9pKbCgE3tg6mb5vr3yLu6a7lzrjmHL2cwy/x6dW6Npe/T0tff5qUC7spM6xm+T8HM35mJ7oNsu+JaDUyfvn6M6RdPMvzL83hxb8tYGinUPQ6rd1jFPVn/8VstsSn2wLcyhvE9Xo9xcXF+Pn5XXVYulOmCUKI24F3AC3wsZTy9ZraX+00wWSEt6af5PLZUfgGnuLpD93wC7I/jSiPh07D7THhtAn5/aUPz7bUh0jPs78MWdX3IhL4CriJm/+YzZ2PpOOmq65tRXTuHpxNzlI+CU7GZJL8Ep/OwUtXquyzRri+/8os/v3vf5OcnMzq1aur7atBbQZCCC0QDwwDEjGv7U2UUh6v7pirUQaz7hiI0fAZ5ipuHwIzAEOtuQWa+bhzZ1xzArzcHb6m640yo4ktp9M5dCmnyj57vhdd+t2Oh9f77FoXSavoIu57MZnA0DIunznOv55/iOLCfIyG35LLurl7EDvgNkY9+hz+wSHERQUwoH0zp8Zu/F4pNhj54XByrYWAn7q1vUMj3oa2GfQBzkgpE6SUpcAK4C4n9AuYv6wLHp+Pj99ZhLgFrdvjwOPoPLS15v7rEObL+N4tf9eKAMwRkkM7hTEyNgIPXcV/uT3fC08fT+55qoA/zU4i5YI7C6a14vhub3au+4qCnCyMhlLzygXm/AhGQ6nNLiMlHLx0hS93XSAxWy1B1of0vBKW7b5YqyJwFs6wGURirhVmJRG4sXKjSuXVHOpYSvjibye4fObfuOsL6NLvdY7u/FeNocrmc0G/NsHc2Cb4aq7nuqV9mC+hvnrWHkkiLfe34X51BWTibs6neZsS/v5gDh/P7oY58OlroAxpMtsh2sb2sSVeKc+VQgOr9iXSrUUAN7Vtpoq51JHTqXmsP57qUCh7WakgOxuCgup3TmdME8YCt0spH7Z8vhe4UUo5vbpjHJkm6PVBlJQsBO4H1mP2H8ioklug8vq5u5uG4TFhtAv1rdd1Xc+UGU1sPpXOkctVpw1WrG7Jox9/ka/eeo2C3NlkpdwJbMfN/QFiB3SqNp6hskuzn6eOoZ1Cad3s+lnBaShMJsn2sxnsPZ9dZZ89V/GcTC1L5jenTbiejRsFtc0UGnqacBloUe5zlGVbvbj55iTgPjTaV4E/oPPIt+UWiGzbiWGTplGQe6XCE8nPU8c9vVooRVALbloNt3YOY3hMODqt/W9P+RWDxNN70bnPBCYBsZSV7qIgZ3C1GZkruzTnFhlYc+AyPxxOVlGQNZBbbODrfZfsKgL47b6u/fhN3n96Csd2FfPWn1uRlODB1KnUqghqwxkjAzfMBsRbMCuBPcAkKeWx6o5xZGSwezc8/NTrHNv9gt3CJeXLoI99ci7NA/TcGdccL/cmt1p6TcnIL2HtoSSyCw1A7SsGGm0nPH1+oCCnDTeNvMKox9Jx95A1HlveyOuh03BT22bERvor78VyJKTns/54KkWlxiojAPv39WFgEcHN4cE5SfzjkVb1NiA6a2lxBObMolrgEynlazW1d3Q1ofvA4XgHNKswlz2xZ0udva4UNVNSZmTD8VROp+aX80zcUOE+l18x8PINYd2nzdi8KoiI1iXc+3wy4TeU1qm0fTNfD4Z0DCEq0PFlSGuCm5UrV143Kd7LjCa2n83kwMVsm/9A5QddxWzdOuBdYCrwEzARra4QQ0lxvZWBUx6jUsp1wDpn9FWeaa9+YFtatBYuqfyF89B7MvbuMaoYSD3wcNMyMrY5+y5ks+20sHgmmlcMrKnay68YAIx6NIP23QtZ/kY4C//ckpEPZzDgLhwqJgOQkVfC13sT6RTuS//2zfDT1+7SPH/+fLZt28Yrr7xyXZSGS80t5v+OpdhyU1QeAZQv9tNn2GggDHMsyQDgdRAvgzTQffAfnSKPy5p4k5OTefPJiVWs1OWXwjw89BhKS+rldaX4jZ6tAhnbK4qi3CxuGjmRtrF9CG/Vnraxfey6J0f3LmTWRxfo0KOQNR+G8u8XI8lKlQ4Vk7FyMiWPJdvPs/V0OsUG+w5knp6eCCGum9JwJpNk59lMVvx6qUIx4JrK76VeCsFdfxyh6QXcAzwP0jy127thDRqNpt73w2Un2PPnz+fMYbMRqnKwTGFuFvc9+Ah/eeJxFi9eTHJy8rUR8jokMsCTrRt+4MejKbXmXQTwDTTy0CtJ7PzBn+8/CsHNfSl9hqcS2Ta/Shk6K5XnxGUmyd7z2RxLyqVP6yBiI/0rpGxPSEjgmWeeYc2aNRQWFjbp0nDJOUX8fDKtwtKulerybZz4tS3nT7yNf5CRoIgnCAwpI+/KQOL3b8dkNKf4H3f3GBYsWFAv2VxOGVROc1a5LmKwjzubf1prS+e9aFH1JckVV4e3hxtjukeyMyGTPeezqi32YkUIuGlkDu26FbLsH+F8/lpzDm3J4+4n0vAJqPq0L7/aUF7RpyYnM+6JiTz+ynsM7dGBrhalEBERgZ+fH8XFxU7xwb8WFBuMbD+TwZHLOTXez/I+Hzv+u5oTe+5hx9pwOnQv4N4Xk/H2mwmYq25Jk8mmNJxxP1xumpCQkMCkSZPw8jIblsoPlVoEeXFPrxYqr38joNEI+rdrxl3dIvF0dyz4SO95Ga1uKLdMPM/RXd7885FWHNriY4tyfPaOrjUmULEqidX/eYfNp9L5bMd5DlzMxmA0kZqaytSpU9m1axdTp06tdyKPxsJkkhy9nMOSHec5nGhWBNb7UXkKnJuZRkHuFYZNfhxPny4knvkX2akjuGVCJo/+7TLefr85IFVO0pOamlpvWV0yn8G0adNYvHgxWjcdZZYlxdl/X8iwzmFNNhFJUya32MCPR5KrzUptHfYHhUeyb+N39LtjAgNG/Y1lb4STeFpPUPhuslLG0OvWPpiMZTbjr5u7B3pPb4oK8jCWGar0ax0NerpriY30J65FgEsmqa2OM2l57DibWcEuABWT2GalXrZNl6zbo/u8yoUTz1FmkASEPM+U5wfw7QevVXA2qowzYhNcUhlYk3UE9fwDm9YsR1uSwy//Z79MmKJxMJkk285ksL/cEpiVp2+PtrknV0QLzALmACXAc8BiQNqGtwC9bv1jBSVR3ZKkm0bQIdyXbi0CrqrATGNxIbOAHWczScmpqDxr8+Ew36+XgZeA4yDGIogntGVb0i6erTHZzHWrDKx8vDWBvm2C6RKpioK6CuWdY2r6cvcYeidDxj3Epq8+5vC2c5QZ3gFuQev2K0bjIyDtR5q6uXs4lGWpma8HMc39iA73c3ga05CUGU2cSM7j4KVsMiqNBKzUXN27ObAMGAR8CkwH7BtwK0fqurtp+POQdg7J2WRrLY6MbU64v+s+AX6PtAnxYfKNHvx0NIWXlmys4PMB2PIw6r188A0I5vSh3RjLMtDq7sBoGI8Q7yPEQZpF/pfstEcpK01F56HHyzeA9t372crFV55PVyYjr4RfTqWz7XQGrYK9aBfqQ9sQn0ZPrpKWV0x8Sj5Hk3IoKq05t4Z1tQCoUN1b63YXJtMnSJM7Gu2DmIyf0qx5K65kpFTr+GUlMsCT4THOMaS6tDJQisA18dXrGNszit1BXqxf6ktZaYntyx074DZ8/IPIzUq3Va4Ka9WOKX9dwK51K8lKvZ/A0P+wY+1I4DAa7UsYSv5DzLAhtpFAeQezyoE5lTGaJAnpBSSkF6ARaUQGetK6mTdRgZ6E+Hg43eVZSkl6fglnUvOJT82zuXHXhvVa3PWe9L9zEnlXsji8dQvwJsayaWjdjhM76BOG3vNHdq0r4cSeX2p0/NJqBH3bBNP7hkCnZe5y6WmCwvX5w513UezmR8/h42p1GS8/vP1g1tukXnyKvOwOePmewWj8C89/8tcKP/rqDG2O4u6moXmAnnA/T4J93An0cifI291hI7SUkjPnL3HvlMnMeWsxJTo/knOKq3WOKk9lRVbZxfj9pz8g6exfKS4Ko3nrHwkMW8RD896yHW/NYJR6KYH87Ex8AoNtoeIz//ERI7pGEHoVdpMmazNQNA1KyoxsOpnOieRcoPbKVVZMJtj/sy+r3tVRWtyMwND9PPJqEAv/3JEyQ9V5t9BoWPDTiXrJqhECH70bep0GvZsWvU6Lu5sGo0maX1JiKDORV2wgr7iMFW/PqbZAbU1Yf/wIUcm4qsdsUJ1FUJiJCbNSaBfreExNdIQvQzuFXXV+iCZrM1A0DTzczKnn24Z4s/FEGlTjSVf5qf7XUVYDpAfwJNlpL/DPR32Bj4BXgbMV2kuTiZm3daw11V1NmKQkt8hAYuJv+RrsLdvVFCdQ07mrGFUrPGz/ACwCWtNjaApjn8xD7+XYw1inFQzuGNqgxnSXczpSNF3ah/lyb79WtAnxrrEKlpXffPEF8AbQFnPw63jgJPCJZZuZ8g5o9aV8vobK5eQqylY1TqAmZry9Am//INzcPWzHBYbdiDlD1DqgmJi+rzLlr7k2RVCdE5KVIG93JvRp2eCrampkoHAqPh5u3NUtknZffc0v8emUGEzVxihU9cXPolnz97iS8R5lpU8C04B7MafVfJuy0v3VRkE6SuUnd+qFM0DVJ391cQK1nduaJxJAqwvBUPIE2Wkz0Wh03DTyBEbj38m/koI52MhMde7ZAJ3Cfbkl+uqnBXVBKQNFgxDT3J+WQV5sPJHK+Qzzerm91YHK+RfNVvQk3NxfoKz0DTy8XsFQMgWTcQq+Qce5fHYFJhNoavltVLcSYV0OrSlfgxV7uSGr67eiktEDj2M0vAAE4x+0mekLWxEcocXsUGTvmIoKaeG6IwzqGEJsVECd7nt9UAZERYNzIjmXLfHpfP7m7FqNcVYrevkf4ISnP2DXj/5s/TaAKxk6giNK6XNbLr1vyyUgxH4atcrW+wr73pnDznUrbcY9q2+EI0bC6vrNzUxj9aJFHN3ZFpNxKhCFX9B+JszS0Kmn/QQu1Rla753xEpMGx17VakFtqNUExTWlciSqlboaAo1lcHirLzvW+XP2kBdCSPTeO7jjwWb0GKpB7yUdSr1W07Jd5QS7VqrrV+Omo3nr0QSFv87hrWGAF0JsQspXuGlkRO3KxaKYrKn9/jDuXlYv/QQPt4ZxnlLKQHFNSU5OrpCPwN1DT5dq0qE5SkaSjs9fPUjimd5AFEKU0L57ATd0Ps+27yZSWnKBstKSGlOv1YXKT3FEe5BjcNNNpszQFUQxyC8IiljDAy8/Vm327spYFdOAO8dzftv3lOZl1VgRqb402NKiEGIcMBeIBvpIKdUvXFGFyvkISktLadO8Gc1CwxyqC1CZZ+/oWs4PQQP0R8rRxO8fQ/z+HsAp4BhCsw1DyWaEaIZv4NUrAoD8HB3H9wRjKJkPDAXZA4Aywy5gJsjPgUyykmHBtHUOj3oemPM+Ib4ejOgaQdAjo+olY32prwHxKDAG88KwQlEt1nwEjz76qC071f033cDW0xmcTMmtNYFKeboNGsHejWssn0zAVstrJtAduA0YhDRNAh5j3//g6A4ToS1LCW9ZQrPmBrz9jXj5GvHyNeHhacJoBKNBUGYQlBRryE7VkZWiIyvVjbREdzKTOmCuDVSCp88pigqeAfk1cLGCbPYMkdWhEYJeNwTSt02wS4TmOys78mbgGUdHBmqaoChPam4xW+LTScyu2ROvthBgc8k3gTQZLTUjh9Nr2DyyUsJJvehO2kV3Ui56kJvp2DNQo8nFZDqL2flpF7Ad2A+U2s5X3ruwLobIQC8dw7uEE+HfuHkcXcID8WrKqyl+H4T56RnXqwUJ6flsO5NRJRmIlcpRkkKjxT84lCvpyeZAKcsP87eakV5EtsnjfysesXkazlz0Fl4+oRTmayjM1VKYp6W4SIObm0TrJtny7WKObP+OkBZuZCQepNetd1XJteDlG0777v24kp5MfnYmeVcy8A1oVsEQWR0aIejWMoCb2ga7XGHaWkcGQoiNgL0YyRellN9Z2mxGjQwUTsBkkpxMyePXc5l2IwIrW98DwyKJ7j2QviPG8+m8PwPwwJxFNgOeb2Azdv6wotYEIRXtEPZxNNdCecr7JXRs3YKh0aGE+l67aNx6jQyklLc6XySFwj4ajaBzcz86hfvaVQr2HIGsHo7lXYV3/983DnkaWrHaIcrnGQCzAvD2C6xTroXyWL0Lj//wCS8v+dhp4cYNgbIZKFwak0lyNj2ffReySc6xn4PRHo5Uhqq+dJmZutgAKlNdv9e68leDFV4VQowWQiQC/YAfhBD/V5/+FIrKaDSC9mG+TOjTkvG9W9A+zAeNA09Xa2yBNUEImH/cZaUlnDm4y9auckCS0GjxCQgmJKp1tcVjakOrESzb8Cvjxk+wZfn28vJi8uTJnDt3rk59NSb1MiBKKb8FvnWSLApFjTQP8KR5gCf5JWWcSM7l2OWcGjMNWacU5T0N87MzSLlwxhYUVDkgyWgoJXbAbXUeCQDodVq6RPoR1yIAP72On5cHNKlaD8oDUdGkScwu5GRyHgkZ+ba6nPaoyU05uvfAKvEQtXkOlqeZjztxLQKIjvCrsEJgzfJd3reiIb0LHUG5Iyuue6SUJOUUcyYtn/MZBWQVVFwZqEuFaEdo5uNOu1BfOoT5EOzj4azLaHBcws9AoWhIhBBEBngSGeDJoA4hFJSUcflKEZezi0jOKcbdLfyq8hNYCfJ2J8JfT3PLOQK93Rv4ihofpQwU1yXeHm50CPOlQ5gvYB45/PROEZPvf5g7x9/Lyi8+Iz01hegIP0qNJrRCoNWYnYJ0bhr89G746XX46nUEeOkaPQX7tUBNExSK3xENtrSoUCiuH5QyUCgUgFIGCoXCglIGCoUCUMpAoVBYUMpAoVAAShkoFAoLShkoFApAKQOFQmFBKQOFQgEoZaBQKCwoZaBQKAClDBQKhYX65kB8QwhxUghxWAjxrRAiwElyKRSKRqa+I4MNQBcpZSwQDzxff5EUCsW1oF7KQEq5XkpZZvm4C4iqv0gKheJa4EybwYPAj07sT6FQNCK1pj1zsLzai0AZsLSGflStRYXChal3eTUhxP3ASOAWWUMONSnlYmAxmNOe1U1MhULR0NQrIaoQ4nbgWWCQlLLQOSIpFIprQX1tBu8DvsAGIcRBIcS/nCCTQqG4BtS3vFo7ZwmiUCiuLcoDUaFQAEoZKBQKC0oZKBQKQCkDhUJhQSkDhUIBKGWgUCgsKGWgUCgApQwUCoUFpQwUCgWglIFCobCglIFCoQCUMlAoFBaUMlAoFIBSBgqFwoJSBgqFAlDKQKFQWFDKQKFQAEoZKBQKC/UtrzbfUlrtoBBivRCiubMEUygUjUt9RwZvSCljpZTdgLXAy/UXSaFQXAvqW14tt9xHb0DVQ1Aomij1yo4MIIR4DbgPyAGG1FsihUJxTRA1FEEyN3CgvJql3fOAXko5p5p+bOXVgI7AKQfkawZkONDuWuLqMrq6fOD6Mrq6fOC4jK2klCH2dtSqDBxFCNESWCel7OKUDs197pVS9nJWfw2Bq8vo6vKB68vo6vKBc2Ss72pC+3If7wJO1qc/hUJx7aivzeB1IURHwARcAKbWXySFQnEtqG95tbudJUg1LG7g/p2Bq8vo6vKB68vo6vKBE2R0ms1AoVA0bZQ7skKhAFxEGQghbhdCnBJCnBFC/NXOfg8hxErL/t1CiBtcTL6ZQojjFtfs/wkhWjWmfI7IWK7d3UIIKYRodOu4IzIKIe6x3MtjQohlriSfEKKlEGKTEOKA5X89opHl+0QIkSaEOFrNfiGEeNci/2EhRI86nUBKeU1fgBY4C7QB3IFDQOdKbR4H/mV5PwFY6WLyDQG8LO+nNaZ8jspoaecLbAF2Ab1cTUagPXAACLR8DnUx+RYD0yzvOwPnG/keDgR6AEer2T8C+BEQQF9gd136d4WRQR/gjJQyQUpZCqzAvExZnruAJZb3q4BbhBDCVeSTUm6SUhZaPu4CohpJNodltDAf+AdQ3JjCWXBExkeARVLKbAApZZqLyScBP8t7fyCpEeVDSrkFyKqhyV3A59LMLiBACBHhaP+uoAwigUvlPidattltI6Usw+z6HNwo0jkmX3kewqydG5NaZbQMGVtIKX9oTMHK4ch97AB0EEJsF0LsEkLc3mjSOSbfXGCKECIRWAc80TiiOUxdv6sVqHdsguI3hBBTgF7AoGstS3mEEBpgIXD/NRalNtwwTxUGYx5dbRFCdJVSXrmWQpVjIvCZlHKBEKIf8IUQoouU0nStBXMGrjAyuAy0KPc5yrLNbhshhBvmIVpmo0jnmHwIIW4FXgRGSSlLGkk2K7XJ6At0ATYLIc5jnk9+38hGREfuYyLwvZTSIKU8B8RjVg6uIt9DwFcAUsqdgB5zTICr4NB3tVoa0wBSjdHDDUgAWvOb4SamUps/U9GA+JWLydcds/Gpvavew0rtN9P4BkRH7uPtwBLL+2aYh7zBLiTfj8D9lvfRmG0GopHv4w1Ub0C8g4oGxF/r1HdjXkgNFzgC81PgLOZoSIBXMD9lwayBvwbOAL8CbVxMvo1AKnDQ8vre1e5hpbaNrgwcvI8C83TmOHAEmOBi8nUGtlsUxUHgtkaWbzmQDBgwj6IewhwCMLXc/Vtkkf9IXf/HygNRoVAArmEzUCgULoBSBgqFAlDKQKFQWFDKQKFQAEoZKBQKC0oZKBQKQCkDhUJhQSkDhUIBwP8DLnrkbYzxgxwAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 288x216 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "with torch.no_grad():\n",
    "    # Initialize plot\n",
    "    f, ax = plt.subplots(1, 1, figsize=(4, 3))\n",
    "\n",
    "    # Plot training data as black stars\n",
    "    ax.plot(train_x.numpy(), train_y.numpy(), 'k*')\n",
    "    # Plot predictive means as blue line\n",
    "    ax.plot(test_x.numpy(), mean.numpy(), 'b')\n",
    "    # Shade between the lower and upper confidence bounds\n",
    "    ax.fill_between(test_x.numpy(), lower.numpy(), upper.numpy(), alpha=0.5)\n",
    "    ax.set_ylim([-3, 3])\n",
    "    ax.legend(['Observed Data', 'Mean', 'Confidence'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
