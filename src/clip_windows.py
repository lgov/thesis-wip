import math

import numpy as np
import torch


def find_nearest_idx(array, value):
    idx = np.searchsorted(array, value, side="left")
    #    print(array.shape, value.item(), idx, array[idx].item(), array[idx-1].item())
    return idx


def clip_periodic_windows(X, Y, eclipse_duration,
                          eclipse_period, start_prim_eclipse_date,
                          start_sec_eclipse_date):
    """
    Assumptions:
    - the secondary eclipse starts at start_prim_eclipse_date + period_sec_eclipse
    """
    max_date = X.max()
    sample_period = X[1] - X[0]

    # Find the next eclipse. Loop over dates here, as the positions in the array will change
    # when we start deleting windows
    # For the second eclipse, subtract one whole period to make the loop easier to program.
    next_prim_eclipse_date, next_sec_eclipse_date = start_prim_eclipse_date, start_sec_eclipse_date - eclipse_period

#    X_clipped = X.clone().detach()
#    Y_clipped = Y.clone().detach()

    X_clipped = X.copy()
    Y_clipped = Y.copy()

# Assume the first eclipse is a primary eclipse
    primary_eclipse = True
    next_eclipse_date = next_prim_eclipse_date

    # Now iterate over the data, first the primary eclipse, then a secondary eclipse
    while next_eclipse_date < max_date:
        # There are gaps in the X-data, so we can not delete a window of x pixels, we have to work based on dates.
        next_eclipse_x_start = find_nearest_idx(X_clipped, next_eclipse_date).item()
        next_eclipse_x_end = next_eclipse_x_start + find_nearest_idx(
            X_clipped[next_eclipse_x_start:], next_eclipse_date + eclipse_duration).item()

        # Now delete the window
        i = {"left": max(int(next_eclipse_x_start), 0),
             "right": max(int(next_eclipse_x_end), 0)}
        #        print(primary_eclipse, next_eclipse_date.item(), X_clipped[next_eclipse_x_start-2:next_eclipse_x_start+2].tolist(), i)
        # X_clipped = torch.cat((X_clipped[:i['left']], X_clipped[i['right']:]))
        # Y_clipped = torch.cat((Y_clipped[:i['left']], Y_clipped[i['right']:]))
        X_clipped = np.concatenate((X_clipped[:i['left']], X_clipped[i['right']:]))
        Y_clipped = np.concatenate((Y_clipped[:i['left']], Y_clipped[i['right']:]))


        # Move to the next period
        primary_eclipse = not primary_eclipse
        if primary_eclipse:
            next_prim_eclipse_date += eclipse_period
            next_eclipse_date = next_prim_eclipse_date
        else:
            next_sec_eclipse_date += eclipse_period
            next_eclipse_date = next_sec_eclipse_date

    return X_clipped, Y_clipped